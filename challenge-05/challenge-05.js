/*
Crie uma variável qualquer, que receba um array com alguns valores aleatórios
- ao menos 5 - (fica por sua conta os valores do array).
*/
var arr = ['Jamile', true, 22, [22, 'setembro', '1996'], 1.15];

/*
Crie uma função que receba um array como parâmetro, e retorne esse array.
*/
var myArr = function (arr) {
  return arr;
};

/*
Imprima o segundo índice do array retornado pela função criada acima.
*/

console.log(myArr(arr)[2]);


/*
Crie uma função que receba dois parâmetros: o primeiro, um array de valores; e o
segundo, um número. A função deve retornar o valor de um índice do array que foi passado
no primeiro parâmetro. O índice usado para retornar o valor, deve ser o número passado no
segundo parâmetro.
*/

var func = function (arr, id) {
  return arr[id];
}

/*
Declare uma variável que recebe um array com 5 valores, de tipos diferentes.
*/
var fiveValues = [true, 'Array', 2.25, undefined, { name: 'Jamile' }];

/*
Invoque a função criada acima, fazendo-a retornar todos os valores do último
array criado.
*/
console.log(func(fiveValues, 0));
console.log(func(fiveValues, 1));
console.log(func(fiveValues, 2));
console.log(func(fiveValues, 3));
console.log(func(fiveValues, 4));

/*
Crie uma função chamada `book`, que recebe um parâmetro, que será o nome do
livro. Dentro dessa função, declare uma variável que recebe um objeto com as
seguintes características:
- esse objeto irá receber 3 propriedades, que serão nomes de livros;
- cada uma dessas propriedades será um novo objeto, que terá outras 3
propriedades:
    - `quantidadePaginas` - Number (quantidade de páginas)
    - `autor` - String
    - `editora` - String
- A função deve retornar o objeto referente ao livro passado por parâmetro.
- Se o parâmetro não for passado, a função deve retornar o objeto com todos
os livros.
*/
var book = function (name) {
  var books = {
    'Uma Breve Historia Do Tempo': {
      quantidadePaginas: 225,
      autor: 'Stephen Hawking',
      editora: 'Novatec',
    },
    'Aprender a aprender': {
      quantidadePaginas: 339,
      autor: 'Barbada Oakley',
      editora: 'Compania das Letras',
    },
    'Entendendo Algoritmos': {
      quantidadePaginas: 283,
      autor: 'Bhargava',
      editora: 'Novatec',
    },
  };

  if (name) {
    return books[name];
  }

  return books;
}

/*
Usando a função criada acima, imprima o objeto com todos os livros.
*/
console.log(book());

/*
Ainda com a função acima, imprima a quantidade de páginas de um livro qualquer,
usando a frase:
"O livro [NOME_DO_LIVRO] tem [X] páginas!"
*/
var bookName = 'Aprender a aprender';

console.log(`O livro ${bookName} tem ${book(bookName).quantidadePaginas} páginas!`);


/*
Ainda com a função acima, imprima o nome do autor de um livro qualquer, usando
a frase:
"O autor do livro [NOME_DO_LIVRO] é [AUTOR]."
*/
console.log(`A autora do livro ${bookName} é  ${book(bookName).autor}.`);


/*
Ainda com a função acima, imprima o nome da editora de um livro qualquer, usando
a frase:
"O livro [NOME_DO_LIVRO] foi publicado pela editora [NOME_DA_EDITORA]."
*/
console.log(`O livro ${bookName} foi publicado pela editora ${book(bookName).editora}`);



