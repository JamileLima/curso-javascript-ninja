(function () {
    /*
    Envolva todo o conteúdo desse arquivo em uma IIFE.
    */

    /*
    Crie um objeto chamado `person`, com as propriedades:
        `name`: String
        `lastname`: String
        `age`: Number
    Preencha cada propriedade com os seus dados pessoais, respeitando o tipo
    de valor para cada propriedade.
    */
    var person = {
        name: 'Carol',
        lastname: 'Danvers',
        age: 29
    };
    console.log('Propriedades de "person":');

    /*
    Mostre no console, em um array, todas as propriedades do objeto acima.
    Não use nenhuma estrutura de repetição, nem crie o array manualmente.
    */
    console.log(Object.keys(person));

    /*
    Crie um array vazio chamado `books`.
    */
    var books = [];

    /*
    Adicione nesse array 3 objetos, que serão 3 livros. Cada livro deve ter a
    seguintes propriedades:
    `name`: String
    `pages`: Number
    */
    books.push({ name: 'Entendendo Algoritmos', pages: 323 }, { name: 'A culpa é das estrelas', pages: 125 }, { name: 'Estrutura de Dados e Algoritmos', pages: 234 });
    console.log('\nLista de livros:');
    /*
    Mostre no console todos os livros.
    */
    for (let index = 0; index < books.length; index++) {
        console.log(books[index]);
    }

    console.log('\nLivro que está sendo removido:');
    /*
    Remova o último livro, e mostre-o no console.
    */
    const lastBook = books.pop();
    console.log(lastBook);

    console.log('\nAgora sobraram somente os livros:');

    /*
    Mostre no console os livros restantes.
    */
    console.log(books);

    /*
    Converta os objetos que ficaram em `books` para strings.
    */
    const booksInStr = JSON.stringify(books, '', ' ');
    console.log('\nLivros em formato string:');

    /*
    Mostre os livros nesse formato no console:
    */
    console.log(booksInStr);

    /*
    Converta os livros novamente para objeto.
    */
    const booksInObj = JSON.parse(booksInStr);
    console.log('\nAgora os livros são objetos novamente:');
    console.log(booksInObj);

    /*
    Mostre no console todas as propriedades e valores de todos os livros,
    no formato abaixo:
        "[PROPRIEDADE]: [VALOR]"
    */
    for (let index = 0; index < books.length; index++) {
        for (prop in books[index]) {
            console.log(`${prop}: ${books[index][prop]}`);
        }
    }

    /*
    Crie um array chamado `myName`. Cada item desse array deve ser uma letra do
    seu nome. Adicione seu nome completo no array.
    */
    var myName = ['j', 'a', 'm', 'i', 'l', 'e'];
    console.log('\nMeu nome é:');

    /*
    Juntando todos os itens do array, mostre no console seu nome.
    */
    console.log(myName.join('')); // Jamile

    /*
    Ainda usando o objeto acima, mostre no console seu nome invertido.
    */
    console.log('\nMeu nome invertido é:');
    console.log(myName.reverse());

    /*
    Mostre todos os itens do array acima, odenados alfabéticamente.
    */
    console.log('\nAgora em ordem alfabética:');
    console.log(myName.sort());
}());